FROM    ubuntu:18.04
ARG     version=6.12
LABEL   version=$version
LABEL   description="Laravel"
LABEL   maintainer="ikado@plaiaundi.net"
 
ARG DEBIAN_FRONTEND=noninteractive  
ARG project=laravel
ARG php=php7.4
 
RUN apt update -y && apt upgrade -y && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:ondrej/php && \
    apt update && apt upgrade -y && \
    apt install -y apache2 git curl
RUN apt install -y $php $php-mysql libapache2-mod-$php && \
    apt install -y php-bcmath php7.1-mcrypt \
        $php-json $php-curl $php-dev $php-gd $php-mbstring $php-zip $php-xml && \
    apt autoremove -y && \
    apt clean && rm -r /var/lib/apt/lists/*
 
RUN /usr/bin/curl -sS https://getcomposer.org/installer |/usr/bin/php && \
    /bin/mv composer.phar /usr/local/bin/composer && \
    /usr/local/bin/composer create-project --prefer-dist laravel/laravel /var/www/laravel $version && \
    /bin/chown www-data:www-data -R /var/www/laravel/storage /var/www/laravel/bootstrap/cache
 
RUN cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/999-default.conf && \
    echo '\
    <VirtualHost *:80> \n\
        ServerAdmin webmaster@localhost \n\
        DocumentRoot /var/www/laravel/public \n\
        \n\
        <Directory "/var/www/laravel/public"> \n\
            AllowOverride All \n\
        </Directory> \n\
    </VirtualHost> \n\
    '> /etc/apache2/sites-available/000-default.conf
 
ENV APACHE_RUN_USER     www-data
ENV APACHE_RUN_GROUP    www-data
ENV APACHE_LOG_DIR      /var/log/apache2
 
EXPOSE 80
 
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
#CMD ["/usr/sbin/apache2ctl"]
