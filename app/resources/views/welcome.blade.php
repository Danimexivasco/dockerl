<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Docker</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            table{
                margin: 0 auto;
            }
            table, td ,tr,th{
                border-collapse: collapse;
                border:2px solid orange;
            }
            th{
                background-color: beige;
            }
            #container{
                display: flex;
                justify-content: space-around;
                width: 35%;
            }
            #formulario{
                margin-top: 5%;
            }
            #formulario input{
                padding: 3%;
            }
            #usuarios{
                text-align: center
            }
        </style>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>


    </head>
    <body>
    
    <p></p>
    <div id="container">
    <div id="formulario">
        {{-- <form action="" method="POST"> --}}
            Nombre<input type="text" id="nombre" value=""><br>
            Apellido <input type="text" id="apellido" value=""><br>
            <button onclick="añadirUsuario($('#nombre').val(),$('#apellido').val())">Añadir</button>
            <button type="submit">Guardar cambios</button>
        {{-- </form> --}}
    </div>
    <div id="usuarios">
    <h1>Usuarios de la bbdd</h1>
    <div id="tablaUsuarios">
           
    </div>
</div>
</div>
    <script>
    function crearTabla(aDatos)
    {
        tabla="<table><tr><th>NOMBRE</th><th>APELLIDO</th></tr>";
        for(i=0;i<aDatos.length;i++)
            {
                tabla+=`<tr>
                            <td>${aDatos[i]['Nombre']}</td>
                            <td >${aDatos[i]['Apellido']}</td>
                            <td ><button>Editar</button></td>
                            <td ><button>Eliminar</button></td>
                        </tr>`
            }
            tabla+="</table>";
            console.log(tabla)
            $('#tablaUsuarios').html(tabla);    
    }

    function añadirUsuario(nombre,apellido){
        $.ajax({
            url: 'http://localhost:7000/api/v1/Users',
            type: 'POST',
            // data: {Nombre:nombre,Apellido:apellido},
            data: `Nombre=${nombre} & Apellido=${apellido}`,
            success: (response)=>{
                console.log(response);

            }
        });
    }
    $(document).ready(function(){
        $.ajax({
            url:   'http://localhost:7000/api/v1/Users',
            type:  'GET',
            success: function (response) {
                console.log(response)
                console.log(response[0]['Nombre'])
                //respuesta=JSON.parse(response[0]);
                crearTabla(response);
                //$("p").text(response[0]);
                //sitioTabla.innerHTML=respuesta;
            }
        });
    })
 
</script>
    </body>
</html>
